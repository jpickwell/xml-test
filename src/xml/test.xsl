<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE stylesheet SYSTEM "https://github.com/skoobiedu/miscfiles/raw/master/xms.dtd">
<xsl:stylesheet version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:m="http://www.w3.org/1998/Math/MathML"
                xmlns:pref="http://www.w3.org/2002/Math/preference"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xsi:schemaLocation="">
  <xsl:output doctype-public="-//W3C//DTD XHTML 2.0 plus MathML 3.0 plus SVG 1.1//EN"
              doctype-system="https://github.com/skoobiedu/miscfiles/raw/master/xhtml-math-svg.dtd"
              encoding="utf-8"
              indent="yes"
              media-type="application/xhtml+xml"
              version="1.0" />
  <xsl:strip-space elements="*" />
  
  <!-- XHTML -->
  <xsl:template match="page">
    <!--xsl:text disable-output-escaping="yes">
&lt;!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN" http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd"[


&lt;!ENTITY % MATHML.prefixed      "INCLUDE"&gt;
&lt;!ENTITY % MATHML.pref.prefixed "INCLUDE"&gt;
]&gt;
</xsl:text-->
    <html dir="{@dir}"
          lang="{@lang}"
          xml:lang="{@lang}">
      <head>
        <meta charset="utf-8" />
        <title>
          <xsl:value-of select="@name" />
        </title>
      </head>
      <body>
        <h1>
          <xsl:value-of select="@name" />
        </h1>
        <p>
          Given the quadratic equation
          <math>
            <mrow>
              <mi>a</mi>
              <mo>&InvisibleTimes;</mo>
              <msup>
                <mi>x</mi>
                <mn>2</mn>
              </msup>
              <mo>+</mo>
              <mi>b</mi>
              <mo>&InvisibleTimes;</mo>
              <mi>x</mi>
              <mo>+</mo>
              <mi>c</mi>
              <mo>=</mo>
              <mi>0</mi>
            </mrow>
          </math>
          , the roots are given by
          <math>
            <mrow>
              <mi>x</mi>
              <mo>=</mo>
              <mfrac>
                <mrow>
                  <mo form="prefix">−</mo>
                  <mi>b</mi>
                  <mo>&PlusMinus;</mo>
                  <msqrt>
                    <msup>
                      <mi>b</mi>
                      <mn>2</mn>
                    </msup>
                    <mo>−</mo>
                    <mn>4</mn>
                    <mo>&InvisibleTimes;</mo>
                    <mi>a</mi>
                    <mo>&InvisibleTimes;</mo>
                    <mi>c</mi>
                  </msqrt>
                </mrow>
                <mrow>
                  <mn>2</mn>
                  <mo>&InvisibleTimes;</mo>
                  <mi>a</mi>
                </mrow>
              </mfrac>
            </mrow>
          </math>
          .
        </p>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>
  <xsl:template match="p">
    <p>
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <!-- MathML -->
  <xsl:template match="math">
    <m:math></m:math>
  </xsl:template>
  
  <!-- SVG -->
</xsl:stylesheet>
